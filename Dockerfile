FROM ubuntu:focal

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y liquidsoap

RUN apt-get install -y libaudio2 libbluray-bdj libcamomile-ocaml-dev libfftw3-bin libfftw3-dev libgd-tools libvisual-0.4-plugins jackd2 liblo-dev file opus-tools pulseaudio libraw1394-doc librsvg2-bin lm-sensors serdi sndiod sordi libsox-fmt-all speex festival mplayer youtube-dl  libaacs0 libgdk-pixbuf2.0-bin gstreamer1.0-plugins-base librsvg2-common va-driver-all vdpau-driver-all mesa-vulkan-drivers vulkan-icd vorbis-tools vorbisgain

RUN mkdir /usr/share/liquidsoap/1.4.1
RUN mv /usr/share/liquidsoap/libs /usr/share/liquidsoap/1.4.1/libs
RUN mv /usr/share/liquidsoap/bin /usr/share/liquidsoap/1.4.1/bin

WORKDIR /app

COPY music/lofi.mp3 music/lofi.mp3
COPY crossfade.liq .
COPY entrypoint.sh .
RUN chmod +x entrypoint.sh

ENTRYPOINT ["/app/entrypoint.sh"]

CMD [ "liquidsoap", "stream.liq" ]
