#!/usr/bin/env sh

if [ "$1" = 'liquidsoap' ]; then

  if [ ! -d 'stream.liq' ]; then
    tee stream.liq <<EOF >/dev/null
set("init.allow_root",true)
set("harbor.bind_addr","0.0.0.0")

security = single("/app/music/lofi.mp3")

radio = input.harbor("harbor",port=8888,password="xxx")

radio = fallback(track_sensitive = false, [radio, security])

radio = compress(radio, attack = 4.0, gain = 7.0, knee = 10.0, ratio = 5.0, release = 600.0, threshold = -18.0, window = 0.7)
radio = normalize(radio, target = -1.0, threshold = -65.0)
radio = limit(radio, threshold = -0.2, attack = 2.0, release = 25.0, window = 0.02)

output.icecast(%mp3(samplerate=44100, stereo=true, bitrate=96, id3v2=true),
    host = "$STREAM_HOST",
    port = $STREAM_PORT,
    user = "$STREAM_USER",
    password = "$STREAM_PASSWORD",
    mount = "$STREAM_MOUNT",
    radio
)
EOF
  fi
  exec "$@"
fi

exec "$@"
